#De forma individual realice un programa en PYTHON, que permita
#1. Registrar datos de personas en un diccionario 10 Ptos.
#2. Recorrer los Nombres registrados en el diccionario 5 Ptos.
#3. Eliminar una persona al ingresar la cedula 5 Ptos.
#4. Consultar el nombre de una persona al registrar la cedula 5 Ptos.
#5. Cambiar el nombre ingresado una cedula por el usuario 10 Ptos.
#Debe utilizar procedimientos. 5 Ptos.
#Debe crear un Menú que contenga las opciones solicitadas y
#adicionalmente las opciones de salir e imprimir la documentación de cada
#procedimiento como se ha visto en clase. 5 Ptos.
#Subir por proyecto a GIT y adjuntar el enlace al aulaenlinea.com 5 Ptos.
'''diccionrio global para guardar personas doc'''


'''metodo general para guardar personas'''

lista_personas =[]

def agregarPersona(cedula, nombre):
     nuevaPersona = {"cedula": cedula, "nombre": nombre, }
     lista_personas.append(nuevaPersona)

'''metodos para recorrer nombres'''

def recorrer_nombres():
     for persona in lista_personas:
         print("Persona: " + persona["nombre"] + "cedula :" + str (persona))
'''metodo para  consultar persona, for para en cada lista de persona cambien su numero de cedula'''
def consultar(cedula):
         for persona in lista_personas:
             if persona["cedula"] == cedula:
                 print("Persona: " + persona["nombre"] + "cedula :" + str (persona))
         return

'''Eliminar nombre por cada cedula que ingrese'''
def eliminar(cedula):
             for persona in lista_personas:
                 if persona["cedula"] == cedula:
                    lista_personas.remove(persona)
                    return

'''modificar el nombre a cada persona con su numero de cedula en cada posicion'''
def modificar(cedula):

    nombre = input("ingrese nuevo nombre")
    i=0
    while i< len(lista_personas):
        if lista_personas[i]["cedula"]==cedula:
            lista_personas[i]["nombre"]=nombre
    return
    i+=1



'''Menu  para ver  opciones'''
def menu():
    print("MENU \n"
          "1) Agregar persona. \n"
          "2) Recorrer Nombres . \n"
          "3) Consultar .\n"
          "4) Eliminar . \n"
          "5) Modificar. \n"
          "6) imprimir documentacio. \n"
          "7) Salir. \n")

    opcion = input("Ingrese el número de acción a realizar: ")

    if opcion == "1":
        cedula=input("digite su numero de cedula:")
        nombre=input("ingrese su nombre:")


        agregarPersona(cedula,nombre)

    elif opcion == "2":
        recorrer_nombres()


    elif opcion == "3":
        nombre= int(input("digite el nombre que deeas consultar:"))

        consultar(nombre)


    elif opcion == "4":
        nombre=int(input("digite el nombre de la persona que desea liminar:"))
        eliminar(nombre)

    elif opcion == "5":
        cedula=int(input("cual numero de cedula dese modificar:"))

        modificar(cedula)
    menu()
menu()

'''imprimir documentacion'''
def imprimir_doc():
    print(menu.__doc__)
    print(agregarPersona.__doc__)
    print(recorrer_nombres.__doc__)
    print(consultar.__doc__)
    print(eliminar.__doc__)
    print(modificar.__doc__)
    print("imprimir_doc__")

    imprimir_doc(menu,agregarPersona,recorrer_nombres,consultar,eliminar,modificar)



